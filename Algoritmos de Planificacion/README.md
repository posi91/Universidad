# Algoritmos de Planificación

Los algoritmos a probar son FcFs, SJF, Round Robin Con Manejo y Round Robin Sin manejo.

## FCFS
Primero en entrar primero en salir. A medida que un proceso pasa al estado listo, este es agregado a la cola de listos.
Cuando el proceso actualmente está ejecutando cesa su ejecución entonces el proceso más
viejo en la cola es seleccionado para correr.

![Alt text](https://www.geeksforgeeks.org/wp-content/uploads/FCFS.png)

## Round Robin
Una interrupción de reloj es generada a intervalos periódicos.Cuando ocurre la 
interrupción, el proceso en la ejecución es seleccionado basado en el esquema FCFS. 
A cada proceso se le da un trozo de tiempo.

![Alt text](https://cdncontribute.geeksforgeeks.org/wp-content/uploads/round-robin-1.jpg)

# SJF "Shortest Job First"
El algoritmo SJF (Shortest-Job-First) se basa en los ciclos de vida de los procesos, los cuales transcurren en dos etapas o períodos que son: ciclos de CPU y ciclos de entrada/salida, tambien conocidos por rafagas.
La palabra shortest (el más corto) se refiere al proceso que tenga el el próximo ciclo de CPU más corto. La idea es escoger entre todos los procesos listos el que tenga su próximo ciclo de CPU más pequeño.
El SJF se puede comportar de dos formas:
Con Desalojo: Si se incorpora un nuevo proceso a la cola de listos y este tiene un ciclo de CPU menor que el ciclo de CPU del proceso que se esté ejecutando,entonces dicho proceso es desalojado y el nuevo proceso toma la CPU.
Sin desalojo: Cuando un proceso toma la CPU, ningun otro proceso podrá apropiarse de ella hasta que que el proceso que la posee termine de ejecutarse.

![Alt text](http://3.bp.blogspot.com/-ScaJInD_u18/VirriDBmhTI/AAAAAAAAGxo/7CaXN9ko038/s1600/captura2.jpg)




