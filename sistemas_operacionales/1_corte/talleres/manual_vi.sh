#!/bin/bash
# Name Script: Manual Vi
# Version: 1.0
# Author: jsposada@poligran.edu.co
# Create: August 11 2018

# Figle imprime banner 
# Despues de esto se crea el fichero manul_vi en el directorio /tmp/ 

figlet VI - Manual v1.0 >/tmp/manual_vi
echo -e "\n Author: jsposada@poligran.edu.co \n Materia: Sistemas Operacionales \n......................................................................\n" >> /tmp/manual_vi
echo -e "\nARGUMENTOS: \n  
		 \n vi --version \t ---> \t Saber version de vi \n
		 \nCOMANDO \n\nNota: A continuacion encontrara el comando en la parte izquierda separado de una flecha en donde encontrara el significado del respectivo comando.\n
		 \n vi \t ---> \t \"Ejecute este comando en una terminal de bash para abrir vi\" 
		 \n vi nombre_de_archivo \t ---> \t  Abrir o crear el archivo
		 \n vi -r nombre_de_archivo \t ---> \t Recuperar un archivo de una caída del sistema
		 \n view nombre_de_archivo \t ---> \t Abrir archivo sólo para leer 
		 \n\nCOMANDOS DEL CURSOR:\n
		 \n h \t ---> \t Moverse un carácter hacia la izquierda 
		 \n j \t ---> \t Moverse una línea hacia abajo 
 		 \n k \t ---> \t Moverse una línea hacia arriba 
 		 \n l \t ---> \t Moverse un carácter a la derecha
 		 \n w \t ---> \t Moverse una palabra a la derecha 
 		 \n W \t ---> \t Moverse una palabra a la derecha (pasados los signos de puntuación)
 		 \n b \t ---> \t Moverse una palabra a la izquierda  
 		 \n B \t ---> \t Moverse una palabra a la izquierda (pasados los signos de puntuación)
		 \n c \t ---> \t Moverse al final de la palabra actual
		 \n Return \t ---> \t Moverse una línea hacia abajo
		 \n Back Space \t ---> \t Moverse un carácter a la izquierda 
		 \n Space Bar \t ---> \t Moverse un carácter a la derecha
		 \n H \t ---> \t Moverse a la parte de arriba de la pantalla 
		 \n M \t ---> \t Moverse al centro de la pantalla
		 \n L \t ---> \t Moverse a la parte inferior de la pantalla
		 \n Ctrl-F \t ---> \t Paginar una pantalla hacia adelante
		 \n Ctrl-D \t ---> \t Desplazarse media pantalla hacia adelante
		 \n Ctrl-B \t ---> \t Paginar una pantalla hacia atrás
		 \n Ctrl-U \t ---> \t Desplazarse media pantalla hacia atrás \n
		 \nINSERTAR CARACTERES Y LINEAS:\n 
		 \n a \t ---> \t Insertar caracteres a la derecha del cursor   
		 \n A \t ---> \t Insertar caracteres al final de la línea
		 \n i \t ---> \t Insertar caracteres a la izquierda del cursor
		 \n I \t ---> \t Insertar caracteres al principio de línea
		 \n o \t ---> \t Insertar una línea por debajo el cursor
		 \n O \t ---> \t Insertar una línea por encima del cursor\n
		 \nCAMBIAR TEXTO:\n
		 \n cw \t ---> \t Cambiar una palabra (o parte de una palabra) a la derecha del cursor 
		 \n c \t ---> \t Cambiar una linea  
		 \n C \t ---> \t Cambiar desde el cursor hasta el final de la línea 
		 \n s \t ---> \t Sustituir cadena por carácter(es) desde el cursor hacia adelante 
		 \n r \t ---> \t Reemplazar el carácter marcado por cursor por otro carácter 
		 \n r Return \t ---> \t Partir una línea
		 \n J \t ---> \t Unir la línea actual con la línea inferior
		 \n xp \t ---> \t Transponer el carácter del cursor con el carácter a la derecha 
		 \n ~ \t ---> \t Cambiar el tipo de letra (mayúscula o minúscula)
		 \n u \t ---> \t Deshacer el comando anterior 
		 \n U \t ---> \t Deshacer todos los cambios en la línea actual
		 \n :u \t ---> \t Deshacer el comando anterior sobre la línea última 
		 \nELIMINAR TEXTO:\n 
		 \n x \t ---> \t Eliminar el carácter del cursor 
		\n X \t ---> \t Eliminar el carácter a la izquierda del cursor 
		\n dw \t ---> \t Eliminar la palabra (o la parte de la palabra a la derecha del cursor) 
		\n dd \t ---> \t Eliminar la línea que contiene al cursor 
		\n D \t ---> \t Eliminar la parte de la línea a la derecha del cursor 
		\n dG \t ---> \t Eliminar hasta el final de línea 
		\n d1G \t ---> \t Eliminar desde el principio del archivo hasta el cursor 
		\n:5,10 d \t ---> \t Eliminar las líneas de la 5 a la 10 
		\nCOPIAR Y MOVER TEXTO:\n
				\n yy \t ---> \t Tirar o copiar línea 
		\n Y \t ---> \t Tirar o copiar línea 
		\n p \t ---> \t Poner la línea tirada o eliminada por debajo de la línea actual 
		\n P \t ---> \t Poner la línea tirada o eliminada por encima de la línea actual  
		\n :1,2 co 3 \t ---> \t Copiar las líneas de la 1 a la 2 y ponerlas después de la línea 3 
		\n :4,5 m 6 \t ---> \t Mover las líneas de la 4 a la 5 y ponerlas después de la línea 6
		\nAJUSTAR LA NUMERACION DE LAS LINEAS:\n
		\n :set nu \t ---> \t Mostrar los números de las líneas 
		\n :set nonu \t ---> \t Esconder los números de las líneas 
		\n  \t ---> \t Establecer la distinción entre mayúsculas y minúsculas 
		\n :set ic \t ---> \t En la búsqueda se ignora la distinción entre mayúsculas y minúsculas 
		\n :set noic \t ---> \t En la búsqueda se distingue entre mayúsculas y minúsculas 
		\nENCONTRAR UNA LINEA:\n 
		\n G \t ---> \t Ir a la última línea del archivo  
		\n 1G \t ---> \t Ir a la primera línea del archivo 
		\n 21G \t ---> \t Ir a la línea 21 
		\nBUSCAR Y REMPLAZAR:\n
		\n /string \t ---> \t Búsqueda de cadena de caracteres 
		\n ?string \t ---> \t Búsqueda hacia atrás de cadena de caracteres
		\n n \t ---> \t Encontrar la siguiente aparición de string en la dirección de búsqueda
		\n N \t ---> \t Encontrar la aparición previa de la cadena de caracteres en la dirección de búsqueda
		\n :g/search/s//replace/g \t ---> \t Buscar y reemplazar
		\nLIMPIAR LA PANTALLA:\n 
		\n Ctrl-L \t ---> \t Limpiar (actualizar) la pantalla 
		\n  \t ---> \t Insertar un archivo en otro archivo 
		\n :r nombre_de_archivo \t ---> \t Insertar (leer) el archivo a continuación del cursor 
		\n :34 r nombre_de_archivo \t ---> \t Insertar el archivo después de la línea 34 
		\nBUSCAR Y SALIR:\n
		\n :w \t ---> \t Guardar los cambios (escribir el contenido de la memoria intermedia) 
		\n :w nombre_de_archivo \t ---> \t Escribir el contenido de la memoria intermedia a un archivo con nombre  
		\n :wq nombre_de_archivo \t ---> \t Guardar los cambios y salir de vi 
		\n ZZ nombre_de_archivo \t ---> \t Guardar los cambios y salir de vi 
		\n :q! nombre_de_archivo \t ---> \t Salir sin guardar los cambios
		 " >>  /tmp/manual_vi
# Por ultimo hacemos un cat al fichero para ver su contenido 
cat /tmp/manual_vi 